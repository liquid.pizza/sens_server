import requests, time, logging
import paho.mqtt.client as mqtt
from influxdb import InfluxDBClient

from config import *

'''
cnt_data is a nested dictionary.
The first key is the sensor type (temp, hum, ...).
For each of this sonsor types cnt_data contains a dictionary with all rooms that have this data as key.
Each room key returns a tuple containing the timestamp it arrived and the data.

e.g.
{
    "temp": {
        "bedroom": (dt, 42.3),
        "kitchen": (dt, 13.37),
    },
    "hum": {
        "livingroom": (dt, 80)
    },
}
'''

def on_msg(client, userdata, msg):
    # topic schema is /<BASE_KEY>/<ROOM_KEY>/<SENSOR_KEY>
    room, sens = msg.topic.split("/")[-2:]
    data = float(msg.payload)
    write_to_db(data, sens, room)

def on_disconnect(client, userdata, rc):
    logging.error("Disconnected")
    client.reconnect()

def write_to_db(val, sens, room):
    point = [{
        "measurement": sens,
        "tags": {
            "room": room
        },
        "fields": {
            "value": float(val)
        }
    }]
    try:
        if influxdb_client.write_points(point, database=DB_NAME):
            logging.debug(f"Wrote {point} to {DB_NAME}@{INFLUX_IP}:{INFLUX_PORT}")
        else:
            logging.warning("Write error\n")
    except InfluxDBClientError as e:
        logging.exception("Error writing to InfluxDB")
    except Exception as e:
        logging.exception("Some thing went wrong")

def main():
    # create new database
    influxdb_client.create_database(DB_NAME)

    mqtt_client = mqtt.Client()

    mqtt_client.connect(BROKER_URL)
    logging.info(f"Connected to {BROKER_URL}")

    # set callbacks
    mqtt_client.on_message = on_msg
    mqtt_client.on_disconnect = on_disconnect

    mqtt_client.subscribe(TOPIC)
    logging.info(f"Subscribed to {TOPIC}")

    run = True

    while run:
        mqtt_client.loop()


# create InfluxDB client
influxdb_client = InfluxDBClient(host=INFLUX_IP, port=INFLUX_PORT)

# define log level
logging.basicConfig(
    level=logging.INFO,
    format="%(levelname)s-%(message)s",
    )

if __name__ == "__main__":
    logging.info("Started")
    main()
    logging.info("Exited")
